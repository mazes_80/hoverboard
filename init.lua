local S = minetest.get_translator and minetest.get_translator("hoverboard")
	or function(s) return s end
local gravity = tonumber(minetest.settings:get("movement_gravity")) or 9.8
local support_bm = minetest.get_modpath("basic_materials")
local support_sd = minetest.get_modpath("screwdriver")
local lock_item = "default:steel_ingot"
local plastic_material = "group:leaves"
local engine_material = "default:mese_crystal_fragment"

if support_bm then
	lock_item = "basic_materials:padlock"
	plastic_material = "basic_materials:plastic_sheet"
	engine_material = "basic_materials:motor"
end

local hoverboard_get_texture = function(self)
	local name = "hoverboard-" .. self.class
	local texture = name .. ".png"
	local color = self.color
	if not color or color == "white" then
		return texture
	elseif color == "dark_green" then
		color = "#004800"
	elseif color == "dark_grey" then
		color = "#484848"
	end
	return texture .. "^(" .. name .. "-color.png^[colorize:" .. color .. ":223)"
end

local hoverboard_get_infotext = function(self)
	local text = self.description
	if self.owner then
		text = text .. " " .. S("locked") .. "\n"
		text = text .. S("Owner") .. ": " .. self.owner
	end
	return text .. "\n\n\n\n\n"
end

local hoverboard_colorize = function(self, puncher, stack)
	-- Do puncher hold dye ?
	if stack:get_name():sub(1,4) ~= "dye:" then
		return
	end
	local color = stack:get_name():sub(5)
	-- Is hoverboard already same color ?
	if self.color == color then
		return
	end
	-- Use dye and update things
	stack:take_item()
	puncher:set_wielded_item(stack)

	self.color = color
	self.object:set_properties({textures = { hoverboard_get_texture(self) }})
	return true
end

local hoverboard_staticdata = function(self)
	local data = {}
	data.color = self.color
	data.owner = self.owner
	return minetest.serialize(data)
end

local hoverboard_lock = function(self, puncher, stack)
	if self.owner or stack:get_name() ~= lock_item then
		return
	end

	stack:take_item()
	puncher:set_wielded_item(stack)

	self.owner = puncher:get_player_name()
	self.object:set_properties({ infotext = hoverboard_get_infotext(self) })
	return true
end

local hoverboard_unlock = function(self, puncher, stack)
	if not self.owner or puncher:get_player_name() ~= self.owner or stack:get_name() ~= "" then
		return
	end

	self.owner = nil
	self.object:set_properties({ infotext = hoverboard_get_infotext(self) })
	minetest.handle_node_drops(puncher:get_pos(), { lock_item }, puncher)
	return true
end

local hoverboard_on_punch = function(self, puncher)
	if not puncher or not puncher:is_player() then
		return
	end
	local name = puncher:get_player_name()
	if self.owner and name ~= self.owner and
		not minetest.get_player_privs(name).protection_bypass then
		return
	end

	local stack = puncher:get_wielded_item()
	if hoverboard_colorize(self, puncher, stack) then
		return
	elseif hoverboard_lock(self, puncher, stack) then
		return
	elseif hoverboard_unlock(self, puncher, stack) then
		return
	end

	if self.driver_name then
		-- when there is a driver, only him can remove hoverboard
		if self.driver_name ~= name then
			return
		end
		-- detach driver and make him stand
		puncher:set_detach()
		player_api.player_attached[name] = nil
		player_api.set_animation(puncher, "stand")
		self.driver_name = nil
	end

	local stack = ItemStack("hoverboard:" .. self.class)
	local meta = stack:get_meta()
	meta:set_string("color", self.color)
	meta:set_string("owner", self.owner)

	stack = puncher:get_inventory():add_item("main", stack)
	if not stack:is_empty() then
		minetest.add_item(self.object:get_pos(), stack)
	end
	self.object:remove()
end

local hoverboard_on_rightclick = function(self, clicker)
	if not clicker or not clicker:is_player() then
		return
	end

	local name = clicker:get_player_name()
	if self.owner and name ~= self.owner and
		not minetest.get_player_privs(name).protection_bypass then
		return
	end

	local collisionbox
	if name == self.driver_name then
		-- driver gets off the vehicle
		self.driver_name = nil

		-- detach the player
		clicker:set_detach()
		player_api.player_attached[name] = nil
		collisionbox = self.collisionbox_model
		-- player should stand again
		player_api.set_animation(clicker, "stand")
	elseif not self.driver_name and not clicker:get_attach() then
		-- a new driver gets in
		self.driver_name = name

		-- attach the driver
		clicker:set_attach(self.object, "", self.attach_point, {x=0, y=0, z=0})
		player_api.player_attached[name] = true
		collisionbox = self.collisionbox_driver

		-- driver have to stand
		minetest.after(0.2, function()
			local player = minetest.get_player_by_name(name)
			if player then
				player_api.set_animation(player, self.drive_anim)
			end
		end)
	end

	-- Set new collision box
	if collisionbox then
		self.object:set_properties({collisionbox = collisionbox,
			selectionbox = self.object:get_properties().selectionbox })
	end
end

-- Set wheels animation speed
local hoverboard_wheel_animation_speed = function(self)
	local sign = 1
	if self.force < 0 then
		sign = -1
	end
	local force = math.abs(self.force)
	force = force / self.force_max
	if self.aspeed == 1 and force == 0 then
		self.aspeed = 0
		self.object:set_animation_frame_speed(0)
	elseif self.aspeed == 0 and force > 0.01 or self.aspeed == 2 and force <= 0.01 then
		self.aspeed = 1
		self.object:set_animation_frame_speed(15*sign)
	elseif self.aspeed == 1 and force > 0.375 or self.aspeed == 3 and force <= 0.375 then
		self.aspeed = 2
		self.object:set_animation_frame_speed(25*sign)
	elseif self.aspeed == 2 and force > 0.75 then
		self.aspeed = 3
		self.object:set_animation_frame_speed(35*sign)
	end
end

local hoverboard_on_step = function(self, dtime, moveresult)
	if not self.driver_name and self.force == 0 then
		if self.aspeed ~= 0 then
			hoverboard_wheel_animation_speed(self)
		end
		return
	end
	local driver,ctrl
	local step_force = 0
	if self.driver_name then
		driver = minetest.get_player_by_name(self.driver_name)
		if driver then
			self.object:set_rotation{x=0, y=driver:get_look_horizontal(), z=0}
			ctrl = driver:get_player_control()
			-- Go forward
			if ctrl.up and not ctrl.down then
				step_force =  100 * dtime
				self.force = math.min( self.force_max, self.force + step_force)
			-- Go backward
			elseif ctrl.down and not ctrl.up then
				step_force = -100 * dtime
				self.force = math.max(-self.force_max, self.force + step_force)
			end
		else
			self.driver_name = nil
		end
	end

	local vel = self.object:get_velocity()
	local strength = math.sqrt(vel.x^2+vel.z^2)

	-- Slowdown
	if step_force == 0 then
		-- When sign changed set force to 0
		if self.force < 0 then
			self.force = self.force + 100 * dtime
			if self.force > 0 then
				self.force = 0
			end
		elseif self.force > 0 then
			self.force = self.force - 100 * dtime
			if self.force < 0 then
				self.force = 0
			end
		end
	end

	-- Lower stepheight when backward
	if self.force < 0 and self.object:get_properties().stepheight ~= 0 then
		self.object:set_properties({stepheight = 0})
	elseif self.force >= 0 and self.object:get_properties().stepheight == 0 then
		self.object:set_properties({stepheight = self.step_height})
	end

	-- Double strength if bike_friendly block under
	local pos = self.object:get_pos()
	pos.y = pos.y + self.collisionbox_model[2] - 0.0125
	local nn = minetest.get_node(pos).name
	local new_strength = math.min(strength, self.speed) + 0.5
	-- This node is not crumbly at all: speed up
	if minetest.get_item_group(nn, "crumbly") == 0 or
		minetest.get_item_group(nn, "bike_friendly") ~= 0 then
		new_strength = math.min(new_strength + self.speed, 2 * self.speed)
	end
	new_strength = new_strength * self.force / self.force_max
	vel = vector.multiply(minetest.yaw_to_dir(self.object:get_yaw()), new_strength)
	hoverboard_wheel_animation_speed(self)

	-- Hoverboard must fall
	if not moveresult.touching_ground and vel.y > -1 then
		vel.y = -gravity
	else
		vel.y = vel.y - 0.5
	end

	self.object:set_velocity(vel)
end

local hoverboard_on_activate = function(self, staticdata, model)
	local data = minetest.deserialize(staticdata)
	if data.color and data.color ~= "" then
		self.color = data.color
	end
	if data.owner and data.owner ~= "" then
		self.owner = data.owner
	end
	self.object:set_acceleration(vector.new(0, -gravity, 0))
	self.object:set_velocity(vector.new())
	self.object:set_animation({x=1,y=60}, 0)
	self.object:set_properties({ textures = { hoverboard_get_texture(self) },
		infotext = hoverboard_get_infotext(self) })
end

local hoverboard_on_place = function(itemstack, user, pointed_thing)
	if pointed_thing.above == nil then
		return nil
	end
	local meta = itemstack:get_meta()
	local data = {}
	data.color = meta:get_string("color")
	data.owner = meta:get_string("owner")

	-- set initial direction.
	local hoverboard = minetest.add_entity(pointed_thing.above, itemstack:get_name(), minetest.serialize(data))
	hoverboard:set_yaw(minetest.dir_to_yaw(vector.subtract(user:get_pos(), hoverboard:get_pos())))

	itemstack:take_item()
	return itemstack
end

local register_hoverboard = function(name, properties)
	minetest.register_craftitem("hoverboard:" .. name, {
		description = properties.description,
		inventory_image = "hoverboard-" .. name .. "_item.png",
		inventory_overlay = "hoverboard-" .. name .. "_item_overlay.png",
		stack_max = 1,

		on_place = hoverboard_on_place,
	})

	local def = {
		initial_properties = {
			physical = true,
			visual = "mesh",
			mesh = "hoverboard-" .. name .. "." .. properties.model_format,
			collide_with_objects = true,
			collisionbox = properties.collisionbox_model,
			selectionbox = properties.select_box,
		},
		on_activate = hoverboard_on_activate,
		on_punch = hoverboard_on_punch,
		on_rightclick = hoverboard_on_rightclick,
		on_step = hoverboard_on_step,
		get_staticdata = hoverboard_staticdata,
		stepheight = properties.step_height,

		class = name,
		aspeed = 0,
		force = 0,
	}
	minetest.register_craft({ output = "hoverboard:" .. name,
		recipe = properties.recipe})

	properties.model_format = nil
	properties.recipe = nil
	for prop,value in pairs(properties) do
		def[prop] = value
	end
	minetest.register_entity("hoverboard:" .. name, def)
end

register_hoverboard("hummer", {
	description = S("Hoverboard Hummer"),
	recipe = {
		{plastic_material, plastic_material, plastic_material},
		{"hoverboard:wheel", "hoverboard:battery",  "hoverboard:wheel"},
	},
	collisionbox_driver = {-0.45,-0.5,-0.45,0.45, 1.4,0.45},
	collisionbox_model  = {-0.45,-0.5,-0.45,0.45,-1/5,0.45},
	select_box = {-3/8,-7/16,-3/8, 3/8,-1/16,3/8},
	attach_point = {x=0, y=-2.5, z=0},
	model_format = "b3d",
	drive_anim = "stand",
	step_height = 0.6,
	force_max = 40,
	speed = 2.2,
})

register_hoverboard("iwheel", {
	description = S("Hoverboard I-wheel"),
	recipe = {
		{plastic_material , "hoverboard:battery", plastic_material},
		{plastic_material , "hoverboard:battery", plastic_material},
		{""               , "hoverboard:wheel"  , ""},
	},
	collisionbox_driver = {-0.4,-0.5,-0.4,0.4, 1.4,0.4},
	collisionbox_model  = {-0.4,-0.5,-0.4,0.4,-1/5,0.4},
	select_box = {-1/8,1/5,-1/8,1/8,-2/5,1/8},
	attach_point = {x=0, y=-2.5, z=0},
	model_format = "b3d",
	drive_anim = "stand",
	step_height = 1.1,
	force_max = 40,
	speed = 2.5,
})

minetest.register_craftitem("hoverboard:battery", {
	description = "Hoverboard Battery",
	inventory_image = "hoverboard-battery.png",
})

minetest.register_craftitem("hoverboard:wheel", {
	description = "Hoverboard Wheel",
	inventory_image = "hoverboard-wheel.png",
})

minetest.register_craft({
	output = "hoverboard:battery",
	recipe = {
		{plastic_material, "default:copper_ingot", plastic_material},
		{""              , "default:stick"       , ""              },
		{"dye:cyan"      , "default:tin_ingot"   , "dye:cyan"      },
	}
})

minetest.register_craft({
	output = "hoverboard:wheel",
	recipe = {
		{"dye:black"     , plastic_material, "dye:black"},
		{plastic_material, engine_material , plastic_material},
		{"dye:black"     , plastic_material, "dye:black"},
	},
})
