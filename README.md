# Hoverboard mod for minetest

## Description

	Add hoverboards to minetest
	* Hummer: controls really to review
	* I-Wheel: Done
	* Scooter: No model yet
	* Auto stabilize model:  No model yet

	It is mostly intended to be used with minetest_game, but other games may be compatible.
    Not ultra laggy but there is really improvements to be done
 
## Licence

	Code under [MIT](license.txt) licence terms
    Artwork under [Free Art License](https://artlibre.org/licence/lal/en/)
